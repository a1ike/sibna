jQuery(function ($) {
  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') ==
          this.pathname.replace(/^\//, '') &&
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length
          ? target
          : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body').animate(
            {
              scrollTop: target.offset().top,
            },
            1000,
            function () {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(':focus')) {
                // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              }
            }
          );
        }
      }
    });

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('.s-header__mob').on('click', function (e) {
    e.preventDefault();

    $('.s-header__row').slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.s-modal').toggle();
  });

  $('.s-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 's-modal__centered') {
      $('.s-modal').hide();
    }
  });

  $('.s-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.s-modal').hide();
  });

  $('.s-home-seo__more').on('click', function (e) {
    e.preventDefault();

    $(this).prev().toggleClass('s-home-seo_hide');
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
      $(this).addClass('s-home-seo__more_active');
      $(this).children('span').html('Скрыть');
    } else {
      $(this).removeClass('s-home-seo__more_active');
      $(this).children('span').html('Читать полностью');
    }
  });

  $('.s-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.s-tabs__header li').removeClass('current');
    $('.s-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  new Swiper('.s-home-projects__cards', {
    navigation: {
      nextEl: '.s-home-projects .swiper-button-next',
      prevEl: '.s-home-projects .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2,
      },
    },
  });

  new Swiper('.s-home-news__cards', {
    navigation: {
      nextEl: '.s-home-news .swiper-button-next',
      prevEl: '.s-home-news .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    },
  });

  new Swiper('.s-papers__cards', {
    navigation: {
      nextEl: '.s-papers .swiper-button-next',
      prevEl: '.s-papers .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 4,
      },
      1200: {
        slidesPerView: 6,
      },
    },
  });

  new Swiper('#smi .s-smi__cards', {
    navigation: {
      nextEl: '#smi .swiper-button-next',
      prevEl: '#smi .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 4,
      },
    },
  });

  new Swiper('#gallery .s-smi__cards', {
    navigation: {
      nextEl: '#gallery .swiper-button-next',
      prevEl: '#gallery .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 4,
      },
    },
  });

  var galleryThumbs = new Swiper('.s-history__thumbs', {
    spaceBetween: 20,
    slidesPerView: 1,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    allowTouchMove: false,
  });
  var galleryTop = new Swiper('.s-history__top', {
    spaceBetween: 20,
    thumbs: {
      swiper: galleryThumbs,
    },
    navigation: {
      nextEl: '.s-history .swiper-button-next',
      prevEl: '.s-history .swiper-button-prev',
    },
  });

  var galleryThumbs2 = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var galleryTop2 = new Swiper('.gallery-top', {
    spaceBetween: 10,
    thumbs: {
      swiper: galleryThumbs2,
    },
  });
});
